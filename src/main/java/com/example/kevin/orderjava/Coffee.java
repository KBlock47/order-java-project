package com.example.kevin.orderjava;
import android.content.Context;
import android.widget.TextView;

import java.text.NumberFormat;


/**
 * Created by Kevin on 2/6/2018.
 */

public class Coffee {

    private static int cupsOfJoe = 0;
    public final static double cupofJoePrice = 5.0;
    private static NumberFormat numberFormat = NumberFormat.getNumberInstance();

    public static int AddCups(int amount, TextView txt, Context context) {
        cupsOfJoe = Math.max(0,cupsOfJoe+amount);

        String orderAmount = context.getResources().getString(R.string.total_order);

        txt.setText(orderAmount + numberFormat.format(cupofJoePrice*cupsOfJoe));
        return cupsOfJoe;
    }

    public static void ResetCups(){
        cupsOfJoe = 0;
    }
    public static int GetCups(){
        return cupsOfJoe;
    }

}
