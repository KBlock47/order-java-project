package com.example.kevin.orderjava;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private Button addCups;
    private Button removeCups;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable((new ColorDrawable(getResources().getColor(R.color.supportColor))));

        addCups = findViewById(R.id.addCup);
        removeCups = findViewById(R.id.removeCup);

    }

    public String GetShortString(int id){
        if (id > 0){
            return getResources().getString(id);
        }else {
            return "";
        }
    }

    public void SetJoe(View view){

        int amount = 1;

        if (view == removeCups) {
            amount = -1;
        }

        TextView order_txt = findViewById(R.id.order_txt);

        Coffee.AddCups(amount,order_txt,this);

        Button order_butt = findViewById(R.id.orderButt);
        String concat = GetShortString(R.string.current_order1) + " " + Coffee.GetCups() + " " + GetShortString(R.string.current_order2);
        order_butt.setText(concat);

        ImageView pic = findViewById(R.id.coffeePic);
        int id = getResources().getIdentifier("cup_"+Coffee.GetCups(),"drawable",getPackageName());

        if (id > 0){
            pic.setImageResource(id);
        }else if(id == 0 & Coffee.GetCups() > 0){
            pic.setImageResource(R.drawable.cup_5);
        }else{
            pic.setImageResource(android.R.color.transparent);
        }
    }

    public void OrderJoe(View view){
       AlertDialog.Builder alert = new AlertDialog.Builder(this);
       NumberFormat price = NumberFormat.getCurrencyInstance();
        String txt = "Ok!";

       if(Coffee.GetCups() > 0) {
           txt = "Order!";
           alert.setMessage("We got your request to order "+Coffee.GetCups()+" cups of coffee which will cost "+price.format(Coffee.GetCups()*Coffee.cupofJoePrice)+"!");
           alert.setTitle(GetShortString(R.string.order_confirm));
           alert.setNegativeButton("Return!", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialogInterface, int i) {

               }
           });

       }else{
           alert.setMessage(GetShortString(R.string.no_coffee));
           alert.setTitle(GetShortString(R.string.order_no));
       }

       alert.setPositiveButton(txt, new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialogInterface, int i) {
               Coffee.ResetCups();
               ImageView pic = findViewById(R.id.coffeePic);
               pic.setImageResource(android.R.color.transparent);

               Button order_butt = findViewById(R.id.orderButt);
               order_butt.setText(GetShortString(R.string.current_order_default));

               TextView order_txt = findViewById(R.id.order_txt);
               order_txt.setText(GetShortString(R.string.total_default));
           }
       });

       alert.show();
    }
}
